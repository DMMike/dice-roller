import React from 'react'
import './App.css'
import Roller from './components/Roller'
import Results from './components/Results'
import Modifiers from './components/Modifiers'

export default class App extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            dice: 1,
            target: 4,
            rolls: [],
            firstRoll: [],
            successes: () => {
                let successes = this.state.rolls.filter(roll => roll >= this.state.target).length
                if (this.state.explode) {
                    const sixes = this.state.rolls.filter(r => r === 6).length
                    if (!sixes) return successes
                    switch (this.state.explode) {
                        case 'double':
                            successes += sixes
                            break
                        case 'd3':
                            for (let i = 0; i < sixes; i++) successes += Math.ceil(this.d6() / 2)
                            break
                        case 'd6':
                            for (let i = 0; i < sixes; i++) successes += this.d6()
                            break
                        default:
                            return successes
                    }
                }
                return successes
            },
            rerolls: {
                type: null,
                values: [],
            },
            explode: null,
        }
    }

    render = () => (
        <div className="App">
            <Roller dice={this.state.dice}
                    target={this.state.target}
                    roll={this.rollDice.bind(this)}
                    updateState={this.updateState.bind(this)}
                    setNextRoll={this.setNextRoll.bind(this)}/>
            <Modifiers rerolls={this.state.rerolls}
                       explode={this.state.explode}
                       updateRerollType={this.updateRerollType.bind(this)}
                       updateExplodeType={explode => this.setState({explode})}
                       toggleValue={this.toggleValue.bind(this)}/>
            <Results results={{rolls: this.state.rolls, successes: this.state.successes.bind(this)}}
                     target={this.state.target}/>
        </div>
    )

    updateState(to) {
        this.setState(to)
    }

    rollDice() {
        let rolls = []
        for (let die = 0; die < this.state.dice; die++) {
            rolls.push(this.d6())
        }

        rolls.sort((a, b) => a - b)
        const firstRoll = [...rolls]

        if (this.state.rerolls.type !== null) {
            const {type, values} = this.state.rerolls
            rolls = rolls.map(r => {
                return (type === '1' && r === 1) || (type === 'fail' && r < this.state.target) || (type === 'other' && values.includes(r))
                    ? this.d6()
                    : r
            })

            rolls.sort((a, b) => a - b)
        }

        this.setState({rolls, firstRoll})
    }

    d6() {
        return Math.ceil(Math.random() * 6)
    }

    setNextRoll() {
        this.setState({dice: Math.max(this.state.successes(), 1)})
    }

    updateRerollType(type) {
        this.setState({rerolls: {...this.state.rerolls, type}})
    }

    toggleValue(value) {
        let values = this.state.rerolls.values
        if (values.includes(value)) values = values.filter(v => v !== value)
        else values.push(value)

        this.setState({rerolls: {...this.state.rerolls, values}})
    }
}