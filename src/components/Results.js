import React from 'react'

export default class Results extends React.Component {
    render = () => (
        <div className="results">
            <div>
                {
                    this.props.results.rolls.map(
                        (roll, index) => {
                            return (
                                <div className="roll">
                                    <span key={index}
                                          className={roll === 6 ? 'critical' : roll >= this.props.target ? 'success' : 'failure'}>
                                        {roll}
                                    </span>
                                    {
                                        index + 1 !== this.props.results.rolls.length
                                            ? (<span>, </span>)
                                            : null
                                    }
                                </div>
                            )
                        }
                    )
                }
            </div>
            {this.props.results.rolls.length ? <div>{this.props.results.successes()} successes</div> : <div>No roll made yet</div>}
        </div>
    )
}