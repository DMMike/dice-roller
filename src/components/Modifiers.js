import React from 'react'

export default class Modifiers extends React.Component {
    render = () => {
        const reroll = this.props.rerolls ?? {},
            type = reroll.type ?? null,
            explode = this.props.explode

        let valueChoices = null;
        if (type === 'other') {
            const values = [],
                curValues = reroll.values ?? []

            for (let v = 1; v <= 6; v++) {
                values.push((
                    <div key={v}>
                        <label>
                            {v}
                            <input onChange={() => this.props.toggleValue(v)}
                                   type="checkbox"
                                   checked={curValues.includes(v)}/>
                        </label>
                    </div>))
            }
            valueChoices = <div className="values-row">{values}</div>
        }

        return (
            <div className="modifiers">
                <div>
                    <h2>Rerolls</h2>
                    <div className="row">
                        <div>
                            <label>
                                <input onChange={() => this.props.updateRerollType(null)}
                                       type="radio"
                                       name="rerolls"
                                       value={null}
                                       checked={type === null}/>
                                None
                            </label>
                        </div>
                        <div>
                            <label>
                                <input onChange={() => this.props.updateRerollType('1')}
                                       type="radio"
                                       name="rerolls"
                                       value="1"
                                       checked={type === '1'}/>
                                1s
                            </label>
                        </div>
                        <div>
                            <label>
                                <input onChange={() => this.props.updateRerollType('fail')}
                                       type="radio"
                                       name="rerolls"
                                       value="fail"
                                       checked={type === 'fail'}/>
                                Failed
                            </label>
                        </div>
                        <div>
                            <label>
                                <input onChange={() => this.props.updateRerollType('other')}
                                       type="radio"
                                       name="rerolls"
                                       value="other"
                                       checked={type === 'other'}/>
                                Other...
                            </label>
                        </div>
                    </div>
                    {valueChoices}
                </div>
                <div>
                    <h2>Explode</h2>
                    <div className="row">
                        <div>
                            <label>
                                <input onChange={() => this.props.updateExplodeType(null)}
                                       type="radio"
                                       name="explode"
                                       value={null}
                                       checked={explode === null}/>
                                None
                            </label>
                        </div>
                        <div>
                            <label>
                                <input onChange={() => this.props.updateExplodeType('double')}
                                       type="radio"
                                       name="explode"
                                       value="double"
                                       checked={explode === 'double'}/>
                                Double
                            </label>
                        </div>
                        <div>
                            <label>
                                <input onChange={() => this.props.updateExplodeType('d3')}
                                       type="radio"
                                       name="explode"
                                       value="d3"
                                       checked={explode === 'd3'}/>
                                D3
                            </label>
                        </div>
                        <div>
                            <label>
                                <input onChange={() => this.props.updateExplodeType('d6')}
                                       type="radio"
                                       name="explode"
                                       value="d6"
                                       checked={explode === 'd6'}/>
                                D6
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}