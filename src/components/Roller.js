import React from 'react'

export default class Roller extends React.Component {
    render = () => {
        return (
            <div className="roller">
                <div>
                    <h2>Dice</h2>
                    <input type="number" min="1" onChange={e => this.setDice(e)} value={this.props.dice}/>
                </div>
                <div>
                    <h2>Target</h2>
                    <input type="number" min="1" onChange={e => this.setTarget(e)} value={this.props.target}/>
                    <div className="or-higher">+</div>
                </div>
                <button onClick={() => this.props.roll()}>Roll</button>
                <button onClick={() => this.props.setNextRoll()}>Use Results</button>
            </div>
        )
    }

    setDice(e) { this.props.updateState({dice: Math.max(Number.parseInt(e.target.value), 1)})}
    setTarget(e) { this.props.updateState({target: Math.min(Math.max(Number.parseInt(e.target.value), 2), 6)})}
}