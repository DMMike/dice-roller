# Dice Roller

This dice roller application serves as a demo on how you can use React.
It is a simple Dice Roller that can be used for games such as *Warhammer: Age of Sigmar*.

The Dice Roller is broken up into three seperate pieces, the Roller, Modifiers, and Results.

## Roller
The Roller is the top part of the application (green).
There are two number inputs and two buttons.

The number inputs determine how many dice will be rolled and what minimum number counts as a success.
The lowest target is 2+ and the highest a 6+.
If there is ever a situation where the target should be 7+, either all results of a 6+ will succeed anyway or all rolls automatically fail.

The 'Roll' button rolls the dice configuration.

The 'Use Results' button sets the 'Dice' value to the number of successes of the last roll (to a minimum of 1).

## Modifiers
There are two basic modifiers (red) that have been implemented, namely 'Rerolls' and 'Exploding Dice'.

### Rerolls
You can choose whether you reroll no dice at all, 1s, all rolls that do not meet or beat the target,
or you can customize what specific dice are rerolled (so for instance, all 6s and all 1s).

All dice are only ever rerolled once.

### Explode
Some abilities allow your dice to 'explode', meaning that they count as multiple successes.
You can choose to not use exploding dice, to double the number of 6s rolled, to count every 6 as D3 successes,
or to count every 6 as D6 successes.

Note that changing any settings after a roll with exploding dice has been made will re-roll any applicable exploding dice.
For instance, if you set 'Explode' to D6 and roll a single 6, then everytime any setting is changed or button is clicked, a new D6 will be rolled for that success.

## Results
Only the final roll, after any rerolls, will be shown in the 'Results' bar (blue).
Any failures will be shown in red, successes in green, and 'criticals' (6s) will be shown in gold.
The number of successes noted at the bottom includes any extra successes from exploding dice.